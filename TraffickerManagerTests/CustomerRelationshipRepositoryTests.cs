using System.Collections.Generic;
using CustomerDomain;
using CustomerDomainContracts;
using FakeItEasy;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class CustomerRelationshipRepositoryTests
    {
        [Test]
        public void CustomerRelationshipRepositoryRequiresAProviderAndAListOfPersisters()
        {
            var provider = A.Fake<IProvideCustomerRelationships>();
            var persisters = new List<IPersistCustomerRelationships>();
            var repository = new CustomerRelationshipRepository(provider,persisters);
        }

        [Test]
        public void GetByIdRetrivesDataFromProviderAndCreatesARelationshipEntityFromIt()
        {
            var provider = A.Fake<IProvideCustomerRelationships>();
            var persisters = new List<IPersistCustomerRelationships>();
            var repository = new CustomerRelationshipRepository(provider, persisters);
            A.CallTo(() => provider.Get(1002)).Returns(new PersistedCustomerRelationship{Id=1000});
            var relationship = repository.Get(1002) as CustomerRelationship;
            A.CallTo(()=>provider.Get(1002)).MustHaveHappened();
            Assert.AreEqual(1000, relationship.GetEntityState().Id, "Relationship data should be set from persister data."); 
        }

        [Test]
        public void GetByIdRetrievesReturnsEmptyRelationshipEntityIfProviderYieldsNull()
        {
            var provider = A.Fake<IProvideCustomerRelationships>();
            var persisters = new List<IPersistCustomerRelationships>();
            var repository = new CustomerRelationshipRepository(provider, persisters);
            A.CallTo(() => provider.Get(1004)).Returns(null);
            var relationship = repository.Get(1004) as CustomerRelationship;
            A.CallTo(() => provider.Get(1004)).MustHaveHappened();
            Assert.AreEqual(1004, relationship.GetEntityState().Id, "Relationship shoudl be empty object."); 
        }

        [Test]
        [ExpectedException(typeof(CustomerRelationshipRepository.InvalidCustomerRelationshipEntityException))]
        public void SaveShouldThrowIfItIsGivenAnIncorrectEntity()
        {
            var persisters = new List<IPersistCustomerRelationships>();
            var repository = new CustomerRelationshipRepository(null, persisters);
            var relationship = A.Fake<ICustomerRelationship>();
            repository.Save(relationship);
        }

        [Test]
        public void SaveShouldPassEntityStateToEachPersister()
        {
            var persisters = new List<IPersistCustomerRelationships>();
            var persister1 = A.Fake<IPersistCustomerRelationships>();
            var persister2 = A.Fake<IPersistCustomerRelationships>();
            persisters.Add(persister1);
            persisters.Add(persister2);
            var repository = new CustomerRelationshipRepository(null, persisters);
            var relationship = A.Fake<CustomerRelationship>();
            var entityState = new PersistedCustomerRelationship();
            A.CallTo(() => relationship.GetEntityState()).Returns(entityState);
            repository.Save(relationship);
            A.CallTo(() => relationship.GetEntityState()).MustHaveHappened();
            A.CallTo(() => persister1.CustomerRelationshipChanged(entityState)).MustHaveHappened();
            A.CallTo(() => persister2.CustomerRelationshipChanged(entityState)).MustHaveHappened();
        }
    }
}