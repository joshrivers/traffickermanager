using System.Linq;
using Core;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class OrderFileReaderTests
    {
        [Test]
        public void ReaderYieldsFourOrders()
        {
            var orderFileReader = new OrderFileReader(TestContext.CurrentContext.TestDirectory);
            var orders = orderFileReader.ReadOrders();
            Assert.AreEqual(4, orders.Count(), "There are four orders in the orders file.");
        }

        [Test]
        public void LastOrderIsPassat()
        {
            var orderFileReader = new OrderFileReader(TestContext.CurrentContext.TestDirectory);
            var lastOrder = orderFileReader.ReadOrders().Last();
            Assert.AreEqual(2003, lastOrder.Id, "Last order in the list has an ID of 2003.");
            Assert.AreEqual(1001, lastOrder.CustomerId, "The passat was ordered by smitty.");
            Assert.AreEqual("Volkswagen", lastOrder.Make, "Car was a VW.");
            Assert.AreEqual("Passat", lastOrder.Model, "A sporty VW.");
            Assert.AreEqual("Green", lastOrder.Color, "Do they make Green passats?");
            Assert.AreEqual(2010, lastOrder.Year, "But it will be hard to refinance, since it's value is going down.");
            Assert.AreEqual("Own", lastOrder.Terms, "It's all his, once the loan is paid.");
        }
    }
}