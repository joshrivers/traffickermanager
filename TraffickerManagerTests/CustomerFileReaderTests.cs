﻿using System.Linq;
using Core;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class CustomerFileReaderTests
    {
        [Test]
        public void ReaderYieldsTwoCustomers()
        {
            var customerFileReader = new CustomerFileReader(TestContext.CurrentContext.TestDirectory);
            var customers = customerFileReader.ReadCustomers();
            Assert.AreEqual(2, customers.Count(), "There are two customers in the customers file.");
        }

        [Test]
        public void FirstCustomerIsId1000()
        {
            var customerFileReader = new CustomerFileReader(TestContext.CurrentContext.TestDirectory);
            var firstCustomer = customerFileReader.ReadCustomers().First();
            Assert.AreEqual(1000, firstCustomer.Id, "First Customer has id 1000.");
            Assert.AreEqual("Bob", firstCustomer.FirstName, "First Customer has is bob.");
            Assert.AreEqual("Jones", firstCustomer.LastName, "First Customer has is Mr Jones.");
            Assert.AreEqual("123 First Street", firstCustomer.StreetAddress, "He lives on 123 First.");
            Assert.AreEqual("Portland", firstCustomer.City, "A portland man!.");
            Assert.AreEqual("OR.", firstCustomer.State, "Definitely an oregonian.");
            Assert.AreEqual("97214", firstCustomer.Zip, "And he can get mail.");
        }
    }
}