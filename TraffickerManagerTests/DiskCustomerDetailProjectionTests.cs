using System.IO;
using Core;
using CustomerDomain;
using CustomerDomainContracts;
using Newtonsoft.Json;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class DiskCustomerDetailProjectionTests
    {
        [Test]
        public void DiskCustomerDetailProjectionNeedsDataDirectoryAndImplementsPersister()
        {
            var dataDirectory = Path.Combine(TestContext.CurrentContext.TestDirectory, "Data");
            var projection = new DiskCustomerDetailProjection(dataDirectory);
            Assert.IsInstanceOf<IPersistCustomerRelationships>(projection);
        }

        [Test]
        public void CustomerRelationshipChangedWritesFileInDataDirectoryNamedByEntityId()
        {
            var dataDirectory = Path.Combine(TestContext.CurrentContext.TestDirectory, "Data");
            if (!Directory.Exists(dataDirectory))
            {
                Directory.CreateDirectory(dataDirectory);
            }
            var projection = new DiskCustomerDetailProjection(dataDirectory);
            projection.CustomerRelationshipChanged(CreateSamplePersistedData());
            var expectedFile = Path.Combine(dataDirectory, "1005.json");
            Assert.IsTrue(File.Exists(expectedFile), "File should be written for entity.");
            var contents = File.ReadAllText(expectedFile);
            var viewModel = JsonConvert.DeserializeObject<dynamic>(contents); 
            Assert.AreEqual("George", viewModel.FirstName.ToString());
            Assert.AreEqual("Washington", viewModel.LastName.ToString());
            Assert.AreEqual(1, viewModel.Orders.Count);
        }


        private PersistedCustomerRelationship CreateSamplePersistedData()
        {
            var data = new PersistedCustomerRelationship
            {
                Id = 1005,
                Details =
                {
                    FirstName = "George",
                    LastName = "Washington",
                    Street = "8121 Potomac",
                    City = "Arlington",
                    State = "VA.",
                    Zip = "01311-1231"
                }
            };
            data.Orders.Add(new Order()
            {
                Id = 2000,
                Make = "Honda",
                Model = "Accord",
                Color = "Blue",
                Year = 2003,
                TransactionType = "Lease"
            });
            return data;
        }
    }
}