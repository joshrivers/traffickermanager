﻿using System;
using System.Collections.Generic;
using Core;
using CustomerDomainContracts;
using FakeItEasy;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class CsvCustomerDomainLoaderTests
    {
        [Test]
        public void CsvCustomerDomainLoaderRequiresReadersAndRepository()
        {
            var customerFileReader = new CustomerFileReader("");
            var orderFileReader = new OrderFileReader("");
            var repository = A.Fake<ICustomerRelationshipRepository>();
            var loader = new CsvCustomerDomainLoader(customerFileReader, orderFileReader, repository);
        }

        [Test]
        public void LoadFilesExecutesCustomerLoadAndOrderLoad()
        {
            var partialLoader = A.Fake<CsvCustomerDomainLoader>();
            A.CallTo(() => partialLoader.LoadOrders()).DoesNothing();
            A.CallTo(() => partialLoader.LoadCustomers()).DoesNothing();
            A.CallTo(() => partialLoader.LoadFiles()).CallsBaseMethod();
            partialLoader.LoadFiles();
            A.CallTo(() => partialLoader.LoadOrders()).MustHaveHappened();
            A.CallTo(() => partialLoader.LoadCustomers()).MustHaveHappened();
        }

        [Test]
        public void LoadOrdersReadsFromOrderFileReaderAndRetrievesCustomerFromRepositoryAndAppliesOrderToItAndSavesIt()
        {
            var customerFileReader = new CustomerFileReader("");
            var orderFileReader = A.Fake<OrderFileReader>();
            var repository = A.Fake<ICustomerRelationshipRepository>();
            var relationship = A.Fake<ICustomerRelationship>();
            var loader = new CsvCustomerDomainLoader(customerFileReader, orderFileReader, repository);
            A.CallTo(() => orderFileReader.ReadOrders()).Returns(new OrderFileReader.CsvOrder[] { new OrderFileReader.CsvOrder(new string[] { "2000", "1000", "Honda", "Accord", "Blue", "2003", "Lease" }) });
            A.CallTo(() => repository.Get(1000)).Returns(relationship);
            loader.LoadOrders();
            A.CallTo(() => orderFileReader.ReadOrders()).MustHaveHappened();
            A.CallTo(() => repository.Get(1000)).MustHaveHappened();
            A.CallTo(() => relationship.AddOrUpdateOrderDetails(2000, "Honda", "Accord", "Blue", 2003, "Lease")).MustHaveHappened();
            A.CallTo(() => repository.Save(relationship)).MustHaveHappened();
        }

        [Test]
        public void LoadCustomersReadsFromCustomerFile()
        {
            var customerFileReader = A.Fake<CustomerFileReader>();
            var orderFileReader = new OrderFileReader("");
            var repository = A.Fake<ICustomerRelationshipRepository>();
            var relationship = A.Fake<ICustomerRelationship>();
            var loader = new CsvCustomerDomainLoader(customerFileReader, orderFileReader, repository);
            A.CallTo(() => customerFileReader.ReadCustomers()).Returns(new CustomerFileReader.CsvCustomer[] { new CustomerFileReader.CsvCustomer(new string[] { "1000", "Bob", "Jones", "123 First Street", "Portland", "OR.", "97124" }) });
            A.CallTo(() => repository.Get(1000)).Returns(relationship); 
            loader.LoadCustomers();
            A.CallTo(() => customerFileReader.ReadCustomers()).MustHaveHappened();
            A.CallTo(() => repository.Get(1000)).MustHaveHappened(); 
            A.CallTo(() => relationship.AddOrUpdateCustomerDetails("Bob","Jones","123 First Street","Portland","OR.","97124")).MustHaveHappened();
            A.CallTo(() => repository.Save(relationship)).MustHaveHappened();
        }
    }
}