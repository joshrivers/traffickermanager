using CustomerDomain;
using CustomerDomainContracts;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class CustomerRelationshipTests
    {
        [Test]
        public void EmptyCustomerRelationshipCanBeCreatedWithId()
        {
            var relationship = new CustomerRelationship(1000);
            Assert.AreEqual(1000, relationship.GetEntityState().Id, "Constructor should set Id.");
            Assert.IsNotNull(relationship.GetEntityState().Details, "Constructor should add empty details object.");
            Assert.IsNotNull(relationship.GetEntityState().Orders, "Constructor should add empty orders list.");
        }

        [Test]
        public void CustomerRelationshipCreatedFromPersistenceDuplicatesObject()
        {
            var data = new PersistedCustomerRelationship { Id = 1005 };
            data.Details.FirstName = "Bob";
            var relationship = new CustomerRelationship(data);
            Assert.AreEqual(1005, relationship.GetEntityState().Id, "Customer Id should be set.");
            Assert.AreEqual("Bob", relationship.GetEntityState().Details.FirstName, "Details should be copied from data.");
            Assert.AreNotSame(data, relationship.GetEntityState(), "Stored data should be a copy.");
        }

        [Test]
        public void AddOrUpdateCustomerDetailsAcceptsChangedDetailData()
        {
            var relationship = new CustomerRelationship(1000);
            relationship.AddOrUpdateCustomerDetails("Bob", "Jones", "123 First Street", "Portland", "OR.", "97124");
            var changedDetails = relationship.GetEntityState().Details;
            Assert.AreEqual("Bob", changedDetails.FirstName, "Firstname is bob.");
            Assert.AreEqual("Jones", changedDetails.LastName, "Surname is Jones.");
            Assert.AreEqual("123 First Street", changedDetails.Street, "He lives on 123 First.");
            Assert.AreEqual("Portland", changedDetails.City, "A portland man!.");
            Assert.AreEqual("OR.", changedDetails.State, "Definitely an oregonian.");
            Assert.AreEqual("97124", changedDetails.Zip, "And he can get mail.");
            Assert.IsTrue(relationship.IsDirty);
        }

        [Test]
        public void AddOrUpdateCustomerDetailsDoesNotSetDirtyFlagIfIdenticalData()
        {
            var relationship = new CustomerRelationship(CreateSamplePersistedData());
            relationship.AddOrUpdateCustomerDetails("George", "Washington", "8121 Potomac", "Arlington", "VA.", "01311-1231");
            var changedDetails = relationship.GetEntityState().Details;
            Assert.AreEqual("George", changedDetails.FirstName, "First name is George.");
            Assert.IsFalse(relationship.IsDirty);
        }

        [Test]
        public void AddOrUpdateOrderDetailsAddsNewOrderIfIdIsUnique()
        {
            var relationship = new CustomerRelationship(CreateSamplePersistedData());
            relationship.AddOrUpdateOrderDetails(2001,"Audi","A4","Black",2006, "Own");
            Assert.AreEqual(2, relationship.GetEntityState().Orders.Count);
            Assert.IsTrue(relationship.IsDirty);
        }

        [Test]
        public void AddOrUpdateOrderDetailsUpdatesExistingOrderIfItExists()
        {
            var relationship = new CustomerRelationship(CreateSamplePersistedData());
            relationship.AddOrUpdateOrderDetails(2000, "Audi", "A4", "Black", 2006, "Own");
            Assert.AreEqual(1, relationship.GetEntityState().Orders.Count);
            Assert.IsTrue(relationship.IsDirty);
        }

        [Test]
        public void AddOrUpdateOrderDetailsDoesNotSetDirtyFlagIfIdenticalData()
        {
            var relationship = new CustomerRelationship(CreateSamplePersistedData());
            relationship.AddOrUpdateOrderDetails(2000, "Honda", "Accord", "Blue", 2003, "Lease");
            Assert.AreEqual(1, relationship.GetEntityState().Orders.Count);
            Assert.IsFalse(relationship.IsDirty);
        }

        private PersistedCustomerRelationship CreateSamplePersistedData()
        {
            var data = new PersistedCustomerRelationship
            {
                Id = 1005,
                Details =
                {
                    FirstName = "George",
                    LastName = "Washington",
                    Street = "8121 Potomac",
                    City = "Arlington",
                    State = "VA.",
                    Zip = "01311-1231"
                }
            };
            data.Orders.Add(new Order()
            {
                Id = 2000,
                Make = "Honda",
                Model = "Accord",
                Color = "Blue",
                Year = 2003,
                TransactionType = "Lease"
            });
            return data;
        }
    }
}