using Core;
using CustomerDomain;
using CustomerDomainContracts;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class InMemoryPersistenceTests
    {
        [Test]
        public void InMemoryPersistenceImplementsPersisterAndProviderInterfaces()
        {
            var persistence = new InMemoryPersistence();
            Assert.IsInstanceOf<IPersistCustomerRelationships>(persistence);
            Assert.IsInstanceOf<IProvideCustomerRelationships>(persistence);
        }

        [Test]
        public void GetReturnsNullForMissingEntity()
        {
            var persistence = new InMemoryPersistence();
            var data = persistence.Get(1031);
            Assert.IsNull(data);
        }

        [Test]
        public void CustomerRelationshipChangedStoresEntityById()
        {
            var persistence = new InMemoryPersistence();
            var data = new PersistedCustomerRelationship {Id = 4121};
            persistence.CustomerRelationshipChanged(data);
            var returnedData = persistence.Get(4121);
            Assert.AreSame(data,returnedData,"This should be an identical object in this implementation.");
        }
    }
}