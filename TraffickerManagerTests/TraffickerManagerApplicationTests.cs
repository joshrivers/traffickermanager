﻿using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core;
using NUnit.Framework;

namespace TraffickerManagerTests
{
    [TestFixture]
    public class TraffickerManagerApplicationTests
    {
        [Test]
        public void TraffickerManagerApplicationRetrievesUserDetail()
        {
            var coreApplication = new TraffickerManagerApplication();
            var result = coreApplication.DetailForCustomer(1000);
            Assert.AreEqual("Bob",result.FirstName);
            Assert.AreEqual("Honda",result.Orders.First().Make);
        }

        [Test]
        public void TraffickerManagerApplicationCreatesDataDirectory()
        {
            var coreApplication = new TraffickerManagerApplication();
            var dataDirectory = Path.Combine(TestContext.CurrentContext.TestDirectory, "Data");
            Assert.IsTrue(Directory.Exists(dataDirectory));
        }
    }
}
