﻿using CustomerDomainContracts;

namespace CustomerDomain
{
    public interface IPersistCustomerRelationships
    {
        void CustomerRelationshipChanged(PersistedCustomerRelationship entity);
    }
}