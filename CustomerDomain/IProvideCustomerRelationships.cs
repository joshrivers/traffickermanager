﻿using System.Collections.Generic;
using CustomerDomainContracts;

namespace CustomerDomain
{
    public interface IProvideCustomerRelationships
    {
        PersistedCustomerRelationship Get(int id);
    }
}