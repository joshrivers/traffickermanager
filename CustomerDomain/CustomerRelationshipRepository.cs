﻿using System;
using System.Collections.Generic;
using CustomerDomainContracts;

namespace CustomerDomain
{
    public class CustomerRelationshipRepository : ICustomerRelationshipRepository
    {
        private readonly IProvideCustomerRelationships _provider;
        private readonly IEnumerable<IPersistCustomerRelationships> _persisters;

        public CustomerRelationshipRepository(IProvideCustomerRelationships provider, IEnumerable<IPersistCustomerRelationships> persisters)
        {
            _provider = provider;
            _persisters = persisters;
        }

        public ICustomerRelationship Get(int id)
        {
            var data = _provider.Get(id);
            if (data != null)
            {
                return new CustomerRelationship(data);
            }
            else
            {
                return new CustomerRelationship(id);
            }
        }
        
        public void Save(ICustomerRelationship entity)
        {
            var customerRelationship = entity as CustomerRelationship;
            if (customerRelationship == null)
            {
                throw new InvalidCustomerRelationshipEntityException();
            }
            else
            {
                var persistedData = customerRelationship.GetEntityState();
                foreach (var persister in _persisters)
                {
                    persister.CustomerRelationshipChanged(persistedData);
                }
            }
        }

        public class InvalidCustomerRelationshipEntityException : Exception
        {
        }
    }
}