using Newtonsoft.Json;

namespace CustomerDomain
{
    public static class JsonCloner
    {
        public static T Clone<T>(this T entity)
        {
            var serializedString = JsonConvert.SerializeObject(entity);
            var clone = JsonConvert.DeserializeObject<T>(serializedString);
            return clone;
        }
    }
}