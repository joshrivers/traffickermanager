﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomerDomainContracts;

namespace CustomerDomain
{
    public class CustomerRelationship : IJsonCloneable, ICustomerRelationship
    {
        private readonly PersistedCustomerRelationship _relationshipData;
        public CustomerRelationship(int id)
        {
            _relationshipData = new PersistedCustomerRelationship { Id = id };
        }

        public CustomerRelationship(PersistedCustomerRelationship savedRelationship)
        {
            _relationshipData = savedRelationship.Clone();
        }

        public bool IsDirty { get; private set; }

        public virtual PersistedCustomerRelationship GetEntityState()
        {
            return _relationshipData.Clone();
        }

        public void AddOrUpdateCustomerDetails(string firstname, string lastname, string street, string city, string state, string zip)
        {
            var customerDetails = _relationshipData.Details;
            IsDirty = IsDirty || customerDetails.FirstName != firstname;
            IsDirty = IsDirty || customerDetails.LastName != lastname;
            IsDirty = IsDirty || customerDetails.Street != street;
            IsDirty = IsDirty || customerDetails.City != city;
            IsDirty = IsDirty || customerDetails.State != state;
            IsDirty = IsDirty || customerDetails.Zip != zip;
            customerDetails.FirstName = firstname;
            customerDetails.LastName = lastname;
            customerDetails.Street = street;
            customerDetails.City = city;
            customerDetails.State = state;
            customerDetails.Zip = zip;
        }

        public void AddOrUpdateOrderDetails(int id, string make, string model, string color, int year,
            string transactionType)
        {
            var order = new Order();
            if (_relationshipData.Orders.Any(o => o.Id == id))
            {
                order = _relationshipData.Orders.Single(o => o.Id == id);
            }
            else
            {
                _relationshipData.Orders.Add(order);
                IsDirty = true;
            }
            IsDirty = IsDirty || order.Make != make;
            IsDirty = IsDirty || order.Model != model;
            IsDirty = IsDirty || order.Color != color;
            IsDirty = IsDirty || order.Year != year;
            IsDirty = IsDirty || order.TransactionType != transactionType;
            order.Id = id;
            order.Make = make;
            order.Model = model;
            order.Color = color;
            order.Year = year;
            order.TransactionType = transactionType;
        }
    }
}