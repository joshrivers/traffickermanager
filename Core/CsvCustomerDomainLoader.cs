﻿using System;
using CustomerDomainContracts;

namespace Core
{
    public class CsvCustomerDomainLoader
    {
        private readonly CustomerFileReader _customerFileReader;
        private readonly OrderFileReader _orderFileReader;
        private readonly ICustomerRelationshipRepository _repository;

        public CsvCustomerDomainLoader(CustomerFileReader customerFileReader, OrderFileReader orderFileReader, ICustomerRelationshipRepository repository)
        {
            _customerFileReader = customerFileReader;
            _orderFileReader = orderFileReader;
            _repository = repository;
        }

        public virtual void LoadFiles()
        {
            LoadOrders();
            LoadCustomers();
        }

        public virtual void LoadOrders()
        {
            foreach (var order in _orderFileReader.ReadOrders())
            {
                var relationship = _repository.Get(order.CustomerId);
                relationship.AddOrUpdateOrderDetails(order.Id,order.Make,order.Model,order.Color,order.Year,order.Terms);
                _repository.Save(relationship);
            }
        }

        public virtual void LoadCustomers()
        {
            foreach (var customer in _customerFileReader.ReadCustomers())
            {
                var relationship = _repository.Get(customer.Id);
                relationship.AddOrUpdateCustomerDetails(customer.FirstName, customer.LastName, customer.StreetAddress, customer.City,customer.State,customer.Zip);
                _repository.Save(relationship);
            }
        }
    }
}