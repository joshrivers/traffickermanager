﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomerDomain;
using Newtonsoft.Json;

namespace Core
{
    public class TraffickerManagerApplication
    {
        public TraffickerManagerApplication()
        {
            var dataDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data");
            if (!Directory.Exists(dataDirectory))
            {
                Directory.CreateDirectory(dataDirectory);
            }
            var dataFile = Path.Combine(dataDirectory, "datafile.txt");

            var customerFileReader = new CustomerFileReader(AppDomain.CurrentDomain.BaseDirectory);
            var orderFileReader = new OrderFileReader(AppDomain.CurrentDomain.BaseDirectory);
            var persistence = new InMemoryPersistence();
            var fileProjection = new DiskCustomerDetailProjection(dataDirectory);
            var projections = new List<IPersistCustomerRelationships>()
            {
                persistence,
                fileProjection
            };
            var repository = new CustomerRelationshipRepository(persistence, projections);
            var loader = new CsvCustomerDomainLoader(customerFileReader, orderFileReader, repository);
            loader.LoadFiles();
        }

        public CustomerProjection DetailForCustomer(int id)
        {
            var dataDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Data");
            var dataFile = Path.Combine(dataDirectory, String.Format("{0}.json",id));
            var result = JsonConvert.DeserializeObject<CustomerProjection>(File.ReadAllText(dataFile));
            return result;
        }
    }
}
