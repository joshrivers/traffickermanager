using System.Collections.Generic;
using CustomerDomain;
using CustomerDomainContracts;

namespace Core
{
    public class InMemoryPersistence : IPersistCustomerRelationships, IProvideCustomerRelationships
    {
        private Dictionary<int,PersistedCustomerRelationship> _storage = new Dictionary<int, PersistedCustomerRelationship>(); 
        public void CustomerRelationshipChanged(PersistedCustomerRelationship entity)
        {
            _storage[entity.Id] = entity;
        }

        public PersistedCustomerRelationship Get(int id)
        {
            if (_storage.ContainsKey(id))
            {
                return _storage[id];
            }
            else
            {
                return null;
            }
        }
    }
}