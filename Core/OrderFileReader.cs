﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Core
{
    public class OrderFileReader
    {
        private readonly string _ordersFile;

        public OrderFileReader(string fileSourceDirectory)
        {
            _ordersFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Orders.txt");
        }

        public virtual IEnumerable<CsvOrder> ReadOrders()
        {
            foreach (var line in File.ReadLines(_ordersFile))
            {
                var splitOrder = line.Split(',');
                if (splitOrder.Length == 7)
                {
                    yield return new CsvOrder(splitOrder);
                }
            }
        }

        public class CsvOrder
        {
            public CsvOrder(string[] fields)
            {
                Id = int.Parse(fields[0]);
                CustomerId = int.Parse(fields[1]);
                Make = fields[2];
                Model = fields[3];
                Color = fields[4];
                Year = int.Parse(fields[5]);
                Terms = fields[6];
            }

            public int Id { get; set; }
            public int CustomerId { get; set; }
            public string Make { get; set; }
            public string Model { get; set; }
            public string Color { get; set; }
            public int Year { get; set; }
            public string Terms { get; set; }
        }
    }
}