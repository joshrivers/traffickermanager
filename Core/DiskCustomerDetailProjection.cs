using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CustomerDomain;
using CustomerDomainContracts;
using Newtonsoft.Json;

namespace Core
{
    public class OrderProjection
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public int Year { get; set; }
        public string Terms { get; set; }
    }

    public class CustomerProjection
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<OrderProjection> Orders { get; set; }
    }

    public class DiskCustomerDetailProjection : IPersistCustomerRelationships
    {
        private readonly string _dataDirectory;

        public DiskCustomerDetailProjection(string dataDirectory)
        {
            _dataDirectory = dataDirectory;
        }

        public void CustomerRelationshipChanged(PersistedCustomerRelationship entity)
        {
            var dataFile = Path.Combine(_dataDirectory, String.Format("{0}.json", entity.Id));
            var projection = new CustomerProjection {FirstName = entity.Details.FirstName, LastName = entity.Details.LastName, Orders = entity.Orders.Select(o=>new OrderProjection {Id = o.Id, Make = o.Make, Model = o.Model, Color = o.Color, Year = o.Year, Terms = o.TransactionType})}; 
            var serializeObject = JsonConvert.SerializeObject(projection);
            File.WriteAllText(dataFile, serializeObject);
        }
    }
}