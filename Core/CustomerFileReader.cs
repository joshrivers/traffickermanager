﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Core
{
    public class CustomerFileReader
    {
        private readonly string _customersFile;

        public CustomerFileReader(string fileSourceDirectory)
        {
            _customersFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Customers.txt");
        }

        public virtual IEnumerable<CsvCustomer> ReadCustomers()
        {
            foreach (var line in File.ReadLines(_customersFile))
            {
                var splitCustomer = line.Split(',');
                if (splitCustomer.Length==7)
                {
                    yield return new CsvCustomer(splitCustomer);
                }
            }
        }

        public class CsvCustomer
        {
            public CsvCustomer(string[] fields)
            {
                Id = int.Parse(fields[0]);
                FirstName = fields[1];
                LastName = fields[2];
                StreetAddress = fields[3];
                City = fields[4];
                State = fields[5];
                Zip = fields[6];
            }

            public int Id { get; private set; }
            public string FirstName { get; private set; }
            public string LastName { get; set; }
            public string StreetAddress { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string Zip { get; set; }
        }
    }
}