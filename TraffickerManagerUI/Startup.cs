﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TraffickerManagerUI.Startup))]
namespace TraffickerManagerUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
