﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TraffickerManagerUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewCustomer(int customerId)
        {
            var customerDetail = MvcApplication.ApplicationCore.DetailForCustomer(customerId);
            return View(customerDetail);
        }
    }
}