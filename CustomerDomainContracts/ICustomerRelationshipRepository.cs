﻿using System.Collections.Generic;

namespace CustomerDomainContracts
{
    public interface ICustomerRelationshipRepository
    {
        ICustomerRelationship Get(int id);
        void Save(ICustomerRelationship entity);
    }
}