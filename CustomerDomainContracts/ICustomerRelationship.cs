﻿namespace CustomerDomainContracts
{
    public interface ICustomerRelationship
    {
        void AddOrUpdateCustomerDetails(string firstname, string lastname, string street, string city, string state, string zip);

        void AddOrUpdateOrderDetails(int id, string make, string model, string color, int year,
            string transactionType);
    }
}