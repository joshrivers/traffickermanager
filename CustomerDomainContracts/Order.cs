﻿namespace CustomerDomainContracts
{
    public class Order
    {
        public int Id { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public int Year { get; set; }
        public string TransactionType { get; set; }
    }
}