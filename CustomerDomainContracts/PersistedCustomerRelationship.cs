﻿using System.Collections.Generic;

namespace CustomerDomainContracts
{
    public class PersistedCustomerRelationship
    {
        public PersistedCustomerRelationship()
        {
            Details = new Customer();
            Orders = new List<Order>();
        }
        public int Id { get; set; }
        public Customer Details { get; set; }
        public List<Order> Orders { get; set; }
    }
}